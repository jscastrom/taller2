var assert = require('assert');
describe('los estudiantes login', function() {
    it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');

        //Test #2: Crear cuenta fallidamente
        browser.waitForVisible('button=Ingresar', 30000);
        browser.click('button=Ingresar');

        var signUp = browser.element('.cajaSignUp');

        var nombre = signUp.element('input[name="nombre"]');
        nombre.click();
        nombre.keys('Juan Sebastian');
        
        var apellido = signUp.element('input[name="apellido"]');
        apellido.click();
        apellido.keys('Castro Morales');
        
        var correo = signUp.element('input[name="correo"]');
        correo.click();
        correo.keys('js.castrom@uniandes.edu.co');

        var esMaestria = signUp.element("label*=maestria");
        esMaestria.click();

        var programa = signUp.element('select[name="idPrograma"]').element('option=Maestría en Ingeniería de Software');
        programa.click();

        var password = signUp.element('input[name="password"]');
        password.click();
        password.keys('1234567890');

        var acepta = signUp.element('input[name="acepta"]'); 
        acepta.click();

        signUp.element('button=Registrarse').click()

        browser.waitForVisible('.sweet-alert', 30000);
        var alertText = browser.element('.sweet-alert').element('h2').getText()

        assert.equal(alertText, "Ocurrió un error activando tu cuenta");
    });
});