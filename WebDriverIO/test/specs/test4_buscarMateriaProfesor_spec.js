describe('los estudiantes login', function() {
    it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');

        //Test #4: Buscar profesor
        var campoBuscar = browser.element('.Select-control').element('span[id="react-select-required_error_checksum--value"]');
        campoBuscar.click();

        var inputBuscar = campoBuscar.element('input');
        inputBuscar.keys('Fernando Lozano');
        inputBuscar.keys(['Enter']);

		var resultadosBusqueda = $('div[class="Select-menu-outer"]').$('div');
		resultadosBusqueda.waitForVisible(30000);

        var profesor = resultadosBusqueda.element('div*=Fernando Enrique Lozano');
        profesor.click();

        //Test #4: Buscar materia
        browser.waitForVisible('.materias', 30000);
      	var materia = browser.element('.materias').element('input[name="id:IELE4014"]')
      	materia.click()
    });
});