describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
      cy.visit('https://losestudiantes.co')
      cy.contains('Cerrar').click()

      cy.screenshot()

      //Test #4: Buscar profesor
      cy.get('.Select-control').find('span[id="react-select-required_error_checksum--value"]').click()
      cy.get('.Select-control').find('span[id="react-select-required_error_checksum--value"]').find('input').type("Fernando Lozano{enter}", {force: true})
      cy.get('.Select-control').get('.Select-menu-outer').contains("Fernando Enrique Lozano").click()

      //Test #4: Buscar materia
      cy.get('.materias').find('input[name="id:IELE4014"]').click()

      cy.screenshot()
    })
})