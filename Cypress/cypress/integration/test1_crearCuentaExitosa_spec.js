describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
      cy.visit('https://losestudiantes.co')
      cy.contains('Cerrar').click()

      cy.screenshot()

      //Test #1: Crear cuenta exitosamente
      cy.contains('Ingresar').click()
      cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Juan Sebastian")
      cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Castro Morales")
      cy.get('.cajaSignUp').find('input[name="correo"]').click().type("js.castrom@uniandes.edu.co")
      cy.get('.cajaSignUp').contains('maestria').click()
      cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
      cy.get('.cajaSignUp').find('input[name="password"]').click().type("1234567890")
      cy.get('.cajaSignUp').find('input[name="acepta"]').click()
      cy.get('.cajaSignUp').contains('Registrarse').click()
      cy.get('.sweet-alert').contains('Registro exitoso')
      cy.get('.sweet-alert').contains('Ok').click()

      //Test #1: Logout
      cy.get('button[id="cuenta"]').click()
      cy.contains('Salir').click()

      //Test #1: Login
      cy.contains('Ingresar').click()
      cy.get('.cajaLogIn').find('input[name="correo"]').click().type("js.castrom@uniandes.edu.co")
      cy.get('.cajaLogIn').find('input[name="password"]').click().type("1234567890")
      cy.get('.cajaLogIn').contains('Ingresar').click()

      //Test #1: Logout
      cy.get('button[id="cuenta"]').click()
      cy.contains('Salir').click()

      cy.screenshot()
    })
})