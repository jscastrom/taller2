describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
      cy.visit('https://losestudiantes.co')
      cy.contains('Cerrar').click()

      cy.screenshot()

      //Test #2: Crear cuenta fallidamente
      cy.contains('Ingresar').click()
      cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Juan Sebastian")
      cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Castro Morales")
      cy.get('.cajaSignUp').find('input[name="correo"]').click().type("js.castrom@uniandes.edu.co")
      cy.get('.cajaSignUp').contains('maestria').click()
      cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
      cy.get('.cajaSignUp').find('input[name="password"]').click().type("1234567890")
      cy.get('.cajaSignUp').find('input[name="acepta"]').click()
      cy.get('.cajaSignUp').contains('Registrarse').click()
      cy.get('.sweet-alert').contains('Ocurrió un error activando tu cuenta')

      cy.screenshot()
    })
})